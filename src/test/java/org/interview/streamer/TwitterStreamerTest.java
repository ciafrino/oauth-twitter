package org.interview.streamer;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import org.interview.models.Tweet;
import org.interview.models.User;
import org.interview.utils.Printer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TwitterStreamerTest {

    private HttpRequestFactory httpRequestFactory;
    private HttpRequestFactory httpRequestFactory2;

    private static final String pattern = "EEE MMM d HH:mm:ss zzz yyyy";
    private SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
    private String json = "{\"created_at\":\"Sat Dec 10 15:22:58 +0000 2016\",\"id\":807606539146104835,\"id_str\":\"807606539146104835\",\"text\":\"testing text bieber\",\"source\":\"\\u003ca href=\\\"http:\\/\\/twitter.com\\/download\\/android\\\" rel=\\\"nofollow\\\"\\u003eTwitter for Android\\u003c\\/a\\u003e\",\"truncated\":false,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":2641246885,\"id_str\":\"2641246885\",\"name\":\"Mario\",\"screen_name\":\"maryh_collins\",\"location\":\"Brasil\",\"url\":null,\"description\":\"Belieber, Lovatic e Harmonizer Sagas: I'm Shadowhunter, Divergent and Tribute. \\nJustin followed me on 01.01.2015\\u2661\",\"protected\":false,\"verified\":false,\"followers_count\":961,\"friends_count\":629,\"listed_count\":13,\"favourites_count\":942,\"statuses_count\":26375,\"created_at\":\"Mon Jul 14 00:36:34 +0000 2014\",\"utc_offset\":null,\"time_zone\":null,\"geo_enabled\":false,\"lang\":\"pt\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"C0DEED\",\"profile_background_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_background_images\\/500814153745833987\\/gIDE5BAn.jpeg\",\"profile_background_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_background_images\\/500814153745833987\\/gIDE5BAn.jpeg\",\"profile_background_tile\":false,\"profile_link_color\":\"223344\",\"profile_sidebar_border_color\":\"FFFFFF\",\"profile_sidebar_fill_color\":\"DDEEF6\",\"profile_text_color\":\"333333\",\"profile_use_background_image\":true,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/766777912926363648\\/AGgEWSw3_normal.jpg\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/766777912926363648\\/AGgEWSw3_normal.jpg\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/2641246885\\/1467216064\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"retweeted_status\":{\"created_at\":\"Sat Dec 03 14:51:27 +0000 2016\",\"id\":805061894323240960,\"id_str\":\"805061894323240960\",\"text\":\"V\\u00eddeo de Justin Bieber performando no 102.7 KIIS FM\\u2019s Jingle Ball em Los Angeles, CA - 02 de Dezembro.\\u2026 https:\\/\\/t.co\\/qxF75KuVcV\",\"display_text_range\":[0,140],\"source\":\"\\u003ca href=\\\"http:\\/\\/twitter.com\\/download\\/android\\\" rel=\\\"nofollow\\\"\\u003eTwitter for Android\\u003c\\/a\\u003e\",\"truncated\":true,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":212830320,\"id_str\":\"212830320\",\"name\":\"Bieber Mania Brasil\",\"screen_name\":\"biebersmaniacom\",\"location\":\"equipebmbr@gmail.com\",\"url\":\"http:\\/\\/www.biebermania.com.br\",\"description\":\"Check out daily updates on the largest source of Justin Bieber's news | Atualiza\\u00e7\\u00f5es di\\u00e1rias na sua maior fonte de not\\u00edcias do Justin Bieber. @Bkstg Crew Member\",\"protected\":false,\"verified\":false,\"followers_count\":30054,\"friends_count\":384,\"listed_count\":128,\"favourites_count\":472,\"statuses_count\":9145,\"created_at\":\"Sun Nov 07 05:05:30 +0000 2010\",\"utc_offset\":-36000,\"time_zone\":\"Hawaii\",\"geo_enabled\":true,\"lang\":\"pt\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"000000\",\"profile_background_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_background_images\\/169172922\\/acoustic_JB.jpg\",\"profile_background_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_background_images\\/169172922\\/acoustic_JB.jpg\",\"profile_background_tile\":true,\"profile_link_color\":\"ABB8C2\",\"profile_sidebar_border_color\":\"C0DEED\",\"profile_sidebar_fill_color\":\"DDEEF6\",\"profile_text_color\":\"333333\",\"profile_use_background_image\":false,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/793998077027905536\\/UzFvPmM2_normal.jpg\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/793998077027905536\\/UzFvPmM2_normal.jpg\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/212830320\\/1478138810\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"is_quote_status\":false,\"extended_tweet\":{\"full_text\":\"V\\u00eddeo de Justin Bieber performando no 102.7 KIIS FM\\u2019s Jingle Ball em Los Angeles, CA - 02 de Dezembro.\\n\\n#MTVStarsJustinBieber https:\\/\\/t.co\\/AXg2p2zJgK\",\"display_text_range\":[0,125],\"entities\":{\"hashtags\":[{\"text\":\"MTVStarsJustinBieber\",\"indices\":[104,125]}],\"urls\":[],\"user_mentions\":[],\"symbols\":[],\"media\":[{\"id\":805061537668931589,\"id_str\":\"805061537668931589\",\"indices\":[126,149],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/805061537668931589\\/pu\\/img\\/uH67auzAXcv9V-7t.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/805061537668931589\\/pu\\/img\\/uH67auzAXcv9V-7t.jpg\",\"url\":\"https:\\/\\/t.co\\/AXg2p2zJgK\",\"display_url\":\"pic.twitter.com\\/AXg2p2zJgK\",\"expanded_url\":\"https:\\/\\/twitter.com\\/biebersmaniacom\\/status\\/805061894323240960\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"large\":{\"w\":240,\"h\":240,\"resize\":\"fit\"},\"small\":{\"w\":240,\"h\":240,\"resize\":\"fit\"},\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":240,\"h\":240,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[1,1],\"duration_millis\":11979,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/vid\\/240x240\\/6KhJrfzYMxKK2KwX.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/pl\\/M6zajbtvBYwgFvDZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/pl\\/M6zajbtvBYwgFvDZ.mpd\"}]}}]},\"extended_entities\":{\"media\":[{\"id\":805061537668931589,\"id_str\":\"805061537668931589\",\"indices\":[126,149],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/805061537668931589\\/pu\\/img\\/uH67auzAXcv9V-7t.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/805061537668931589\\/pu\\/img\\/uH67auzAXcv9V-7t.jpg\",\"url\":\"https:\\/\\/t.co\\/AXg2p2zJgK\",\"display_url\":\"pic.twitter.com\\/AXg2p2zJgK\",\"expanded_url\":\"https:\\/\\/twitter.com\\/biebersmaniacom\\/status\\/805061894323240960\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"large\":{\"w\":240,\"h\":240,\"resize\":\"fit\"},\"small\":{\"w\":240,\"h\":240,\"resize\":\"fit\"},\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":240,\"h\":240,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[1,1],\"duration_millis\":11979,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/vid\\/240x240\\/6KhJrfzYMxKK2KwX.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/pl\\/M6zajbtvBYwgFvDZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/805061537668931589\\/pu\\/pl\\/M6zajbtvBYwgFvDZ.mpd\"}]}}]}},\"retweet_count\":195,\"favorite_count\":98,\"entities\":{\"hashtags\":[],\"urls\":[{\"url\":\"https:\\/\\/t.co\\/qxF75KuVcV\",\"expanded_url\":\"https:\\/\\/twitter.com\\/i\\/web\\/status\\/805061894323240960\",\"display_url\":\"twitter.com\\/i\\/web\\/status\\/8\\u2026\",\"indices\":[104,127]}],\"user_mentions\":[],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"filter_level\":\"low\",\"lang\":\"pt\"},\"is_quote_status\":false,\"retweet_count\":0,\"favorite_count\":0,\"entities\":{\"hashtags\":[],\"urls\":[{\"url\":\"\",\"expanded_url\":null,\"indices\":[125,125]}],\"user_mentions\":[{\"screen_name\":\"biebersmaniacom\",\"name\":\"Bieber Mania Brasil\",\"id\":212830320,\"id_str\":\"212830320\",\"indices\":[3,19]}],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"filter_level\":\"low\",\"lang\":\"pt\",\"timestamp_ms\":\"1481383378193\"}";
    private String json2 = "{\"created_at\":\"Sat Dec 10 16:06:40 +0000 2016\",\"id\":807617536414126080,\"id_str\":\"807617536414126080\",\"text\":\"test\",\"display_text_range\":[0,56],\"source\":\"\\u003ca href=\\\"https:\\/\\/mobile.twitter.com\\\" rel=\\\"nofollow\\\"\\u003eMobile Web (M5)\\u003c\\/a\\u003e\",\"truncated\":false,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":2264508682,\"id_str\":\"2264508682\",\"name\":\"test\",\"screen_name\":\"dotadobieber\",\"location\":\"daddy follows 04.01.15\",\"url\":null,\"description\":\"\\u00a4 purposquad \\u00a4\",\"protected\":false,\"verified\":false,\"followers_count\":1260,\"friends_count\":820,\"listed_count\":55,\"favourites_count\":10716,\"statuses_count\":77790,\"created_at\":\"Mon Jan 06 00:58:03 +0000 2014\",\"utc_offset\":16200,\"time_zone\":\"Kabul\",\"geo_enabled\":true,\"lang\":\"pt\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"000000\",\"profile_background_image_url\":\"http:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_image_url_https\":\"https:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_tile\":false,\"profile_link_color\":\"1B95E0\",\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"000000\",\"profile_text_color\":\"000000\",\"profile_use_background_image\":false,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/793974716323299328\\/ntcHkcqu_normal.jpg\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/793974716323299328\\/ntcHkcqu_normal.jpg\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/2264508682\\/1478133297\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"quoted_status_id\":807483400894509060,\"quoted_status_id_str\":\"807483400894509060\",\"quoted_status\":{\"created_at\":\"Sat Dec 10 07:13:39 +0000 2016\",\"id\":807483400894509060,\"id_str\":\"807483400894509060\",\"text\":\"A rare video of Justin Bieber and his crew backstage tonight at the #iHeartJingleBall in New York City, New York. (\\u2026 https:\\/\\/t.co\\/4Zq3JXjvwa\",\"display_text_range\":[0,140],\"source\":\"\\u003ca href=\\\"http:\\/\\/twitter.com\\/download\\/iphone\\\" rel=\\\"nofollow\\\"\\u003eTwitter for iPhone\\u003c\\/a\\u003e\",\"truncated\":true,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":1239313056,\"id_str\":\"1239313056\",\"name\":\"JustinBieberCrew.com\",\"screen_name\":\"JBCrewdotcom\",\"location\":\"Backup account: @JBCGiveaway\",\"url\":\"http:\\/\\/Instagram.com\\/jbcrewdotcom\",\"description\":\"Your ultimate daily resource for the most recent Justin Bieber news, photos, videos, giveaways, live updates and more! | Contact: justinbiebercrew.com@gmail.com\",\"protected\":false,\"verified\":false,\"followers_count\":479489,\"friends_count\":276,\"listed_count\":2114,\"favourites_count\":21069,\"statuses_count\":151083,\"created_at\":\"Sun Mar 03 16:46:37 +0000 2013\",\"utc_offset\":0,\"time_zone\":\"London\",\"geo_enabled\":true,\"lang\":\"en\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"9266CC\",\"profile_background_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_background_images\\/486873178321338368\\/J8FLD7xX.png\",\"profile_background_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_background_images\\/486873178321338368\\/J8FLD7xX.png\",\"profile_background_tile\":false,\"profile_link_color\":\"9266CC\",\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"DDEEF6\",\"profile_text_color\":\"333333\",\"profile_use_background_image\":false,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/701867021168992257\\/h-yqjmzw_normal.png\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/701867021168992257\\/h-yqjmzw_normal.png\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/1239313056\\/1466259494\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"is_quote_status\":false,\"extended_tweet\":{\"full_text\":\"A rare video of Justin Bieber and his crew backstage tonight at the #iHeartJingleBall in New York City, New York. (December 9) https:\\/\\/t.co\\/ljskxBvQB9\",\"display_text_range\":[0,126],\"entities\":{\"hashtags\":[{\"text\":\"iHeartJingleBall\",\"indices\":[68,85]}],\"urls\":[],\"user_mentions\":[],\"symbols\":[],\"media\":[{\"id\":807483076792217600,\"id_str\":\"807483076792217600\",\"indices\":[127,150],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"url\":\"https:\\/\\/t.co\\/ljskxBvQB9\",\"display_url\":\"pic.twitter.com\\/ljskxBvQB9\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":600,\"h\":338,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":191,\"resize\":\"fit\"},\"large\":{\"w\":1024,\"h\":576,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[16,9],\"duration_millis\":45008,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/320x180\\/nhPwvFEfEYh7OYVI.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.mpd\"},{\"bitrate\":832000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/640x360\\/cm9MbYJ3TJwVFY1g.mp4\"},{\"bitrate\":2176000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/1280x720\\/smYhN_8o54BR_JS4.mp4\"}]}}]},\"extended_entities\":{\"media\":[{\"id\":807483076792217600,\"id_str\":\"807483076792217600\",\"indices\":[127,150],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"url\":\"https:\\/\\/t.co\\/ljskxBvQB9\",\"display_url\":\"pic.twitter.com\\/ljskxBvQB9\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":600,\"h\":338,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":191,\"resize\":\"fit\"},\"large\":{\"w\":1024,\"h\":576,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[16,9],\"duration_millis\":45008,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/320x180\\/nhPwvFEfEYh7OYVI.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.mpd\"},{\"bitrate\":832000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/640x360\\/cm9MbYJ3TJwVFY1g.mp4\"},{\"bitrate\":2176000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/1280x720\\/smYhN_8o54BR_JS4.mp4\"}]}}]}},\"retweet_count\":951,\"favorite_count\":1643,\"entities\":{\"hashtags\":[{\"text\":\"iHeartJingleBall\",\"indices\":[68,85]}],\"urls\":[{\"url\":\"https:\\/\\/t.co\\/4Zq3JXjvwa\",\"expanded_url\":\"https:\\/\\/twitter.com\\/i\\/web\\/status\\/807483400894509060\",\"display_url\":\"twitter.com\\/i\\/web\\/status\\/8\\u2026\",\"indices\":[117,140]}],\"user_mentions\":[],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"filter_level\":\"low\",\"lang\":\"en\"},\"is_quote_status\":true,\"retweet_count\":0,\"favorite_count\":0,\"entities\":{\"hashtags\":[],\"urls\":[{\"url\":\"https:\\/\\/t.co\\/njuzZB56A0\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\",\"display_url\":\"twitter.com\\/JBCrewdotcom\\/s\\u2026\",\"indices\":[57,80]}],\"user_mentions\":[],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"filter_level\":\"low\",\"lang\":\"en\",\"timestamp_ms\":\"1481386000146\"}";
    private String json3 = "{\"created_at\":\"Sat Dec 10 16:06:40 +0000 2015\",\"id\":807617536414126084,\"id_str\":\"807617536414126084\",\"text\":\"test\",\"display_text_range\":[0,56],\"source\":\"\\u003ca href=\\\"https:\\/\\/mobile.twitter.com\\\" rel=\\\"nofollow\\\"\\u003eMobile Web (M5)\\u003c\\/a\\u003e\",\"truncated\":false,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":2264508682,\"id_str\":\"2264508682\",\"name\":\"test\",\"screen_name\":\"dotadobieber\",\"location\":\"daddy follows 04.01.15\",\"url\":null,\"description\":\"\\u00a4 purposquad \\u00a4\",\"protected\":false,\"verified\":false,\"followers_count\":1260,\"friends_count\":820,\"listed_count\":55,\"favourites_count\":10716,\"statuses_count\":77790,\"created_at\":\"Mon Jan 06 00:58:03 +0000 2014\",\"utc_offset\":16200,\"time_zone\":\"Kabul\",\"geo_enabled\":true,\"lang\":\"pt\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"000000\",\"profile_background_image_url\":\"http:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_image_url_https\":\"https:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_tile\":false,\"profile_link_color\":\"1B95E0\",\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"000000\",\"profile_text_color\":\"000000\",\"profile_use_background_image\":false,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/793974716323299328\\/ntcHkcqu_normal.jpg\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/793974716323299328\\/ntcHkcqu_normal.jpg\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/2264508682\\/1478133297\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"quoted_status_id\":807483400894509060,\"quoted_status_id_str\":\"807483400894509060\",\"quoted_status\":{\"created_at\":\"Sat Dec 10 07:13:39 +0000 2016\",\"id\":807483400894509060,\"id_str\":\"807483400894509060\",\"text\":\"A rare video of Justin Bieber and his crew backstage tonight at the #iHeartJingleBall in New York City, New York. (\\u2026 https:\\/\\/t.co\\/4Zq3JXjvwa\",\"display_text_range\":[0,140],\"source\":\"\\u003ca href=\\\"http:\\/\\/twitter.com\\/download\\/iphone\\\" rel=\\\"nofollow\\\"\\u003eTwitter for iPhone\\u003c\\/a\\u003e\",\"truncated\":true,\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":1239313056,\"id_str\":\"1239313056\",\"name\":\"JustinBieberCrew.com\",\"screen_name\":\"JBCrewdotcom\",\"location\":\"Backup account: @JBCGiveaway\",\"url\":\"http:\\/\\/Instagram.com\\/jbcrewdotcom\",\"description\":\"Your ultimate daily resource for the most recent Justin Bieber news, photos, videos, giveaways, live updates and more! | Contact: justinbiebercrew.com@gmail.com\",\"protected\":false,\"verified\":false,\"followers_count\":479489,\"friends_count\":276,\"listed_count\":2114,\"favourites_count\":21069,\"statuses_count\":151083,\"created_at\":\"Sun Mar 03 16:46:37 +0000 2013\",\"utc_offset\":0,\"time_zone\":\"London\",\"geo_enabled\":true,\"lang\":\"en\",\"contributors_enabled\":false,\"is_translator\":false,\"profile_background_color\":\"9266CC\",\"profile_background_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_background_images\\/486873178321338368\\/J8FLD7xX.png\",\"profile_background_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_background_images\\/486873178321338368\\/J8FLD7xX.png\",\"profile_background_tile\":false,\"profile_link_color\":\"9266CC\",\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"DDEEF6\",\"profile_text_color\":\"333333\",\"profile_use_background_image\":false,\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/701867021168992257\\/h-yqjmzw_normal.png\",\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/701867021168992257\\/h-yqjmzw_normal.png\",\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/1239313056\\/1466259494\",\"default_profile\":false,\"default_profile_image\":false,\"following\":null,\"follow_request_sent\":null,\"notifications\":null},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"is_quote_status\":false,\"extended_tweet\":{\"full_text\":\"A rare video of Justin Bieber and his crew backstage tonight at the #iHeartJingleBall in New York City, New York. (December 9) https:\\/\\/t.co\\/ljskxBvQB9\",\"display_text_range\":[0,126],\"entities\":{\"hashtags\":[{\"text\":\"iHeartJingleBall\",\"indices\":[68,85]}],\"urls\":[],\"user_mentions\":[],\"symbols\":[],\"media\":[{\"id\":807483076792217600,\"id_str\":\"807483076792217600\",\"indices\":[127,150],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"url\":\"https:\\/\\/t.co\\/ljskxBvQB9\",\"display_url\":\"pic.twitter.com\\/ljskxBvQB9\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":600,\"h\":338,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":191,\"resize\":\"fit\"},\"large\":{\"w\":1024,\"h\":576,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[16,9],\"duration_millis\":45008,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/320x180\\/nhPwvFEfEYh7OYVI.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.mpd\"},{\"bitrate\":832000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/640x360\\/cm9MbYJ3TJwVFY1g.mp4\"},{\"bitrate\":2176000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/1280x720\\/smYhN_8o54BR_JS4.mp4\"}]}}]},\"extended_entities\":{\"media\":[{\"id\":807483076792217600,\"id_str\":\"807483076792217600\",\"indices\":[127,150],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/ext_tw_video_thumb\\/807483076792217600\\/pu\\/img\\/0hnbY4A9QZyYpOtW.jpg\",\"url\":\"https:\\/\\/t.co\\/ljskxBvQB9\",\"display_url\":\"pic.twitter.com\\/ljskxBvQB9\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\\/video\\/1\",\"type\":\"video\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":600,\"h\":338,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":191,\"resize\":\"fit\"},\"large\":{\"w\":1024,\"h\":576,\"resize\":\"fit\"}},\"video_info\":{\"aspect_ratio\":[16,9],\"duration_millis\":45008,\"variants\":[{\"bitrate\":320000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/320x180\\/nhPwvFEfEYh7OYVI.mp4\"},{\"content_type\":\"application\\/x-mpegURL\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.m3u8\"},{\"content_type\":\"application\\/dash+xml\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/pl\\/SeVkzrS_GnU105XZ.mpd\"},{\"bitrate\":832000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/640x360\\/cm9MbYJ3TJwVFY1g.mp4\"},{\"bitrate\":2176000,\"content_type\":\"video\\/mp4\",\"url\":\"https:\\/\\/video.twimg.com\\/ext_tw_video\\/807483076792217600\\/pu\\/vid\\/1280x720\\/smYhN_8o54BR_JS4.mp4\"}]}}]}},\"retweet_count\":951,\"favorite_count\":1643,\"entities\":{\"hashtags\":[{\"text\":\"iHeartJingleBall\",\"indices\":[68,85]}],\"urls\":[{\"url\":\"https:\\/\\/t.co\\/4Zq3JXjvwa\",\"expanded_url\":\"https:\\/\\/twitter.com\\/i\\/web\\/status\\/807483400894509060\",\"display_url\":\"twitter.com\\/i\\/web\\/status\\/8\\u2026\",\"indices\":[117,140]}],\"user_mentions\":[],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"filter_level\":\"low\",\"lang\":\"en\"},\"is_quote_status\":true,\"retweet_count\":0,\"favorite_count\":0,\"entities\":{\"hashtags\":[],\"urls\":[{\"url\":\"https:\\/\\/t.co\\/njuzZB56A0\",\"expanded_url\":\"https:\\/\\/twitter.com\\/JBCrewdotcom\\/status\\/807483400894509060\",\"display_url\":\"twitter.com\\/JBCrewdotcom\\/s\\u2026\",\"indices\":[57,80]}],\"user_mentions\":[],\"symbols\":[]},\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"filter_level\":\"low\",\"lang\":\"en\",\"timestamp_ms\":\"1481386000146\"}";

    @Before
    public void setUp() throws Exception {

        // Setting up th environment for mocking. Using MOckito v2 in order to mock final classes such as
        // HttpRequestFactory and HttpRequest.

        httpRequestFactory = Mockito.mock(HttpRequestFactory.class);
        HttpRequest httpRequest = Mockito.mock(HttpRequest.class);
        HttpResponse httpResponse = Mockito.mock(HttpResponse.class);

        InputStream inputStream = new ByteArrayInputStream((json + "\n" + json2 + "\n").getBytes());

        Mockito.when(httpRequestFactory.buildPostRequest(Mockito.any(), Mockito.any())).thenReturn(httpRequest);
        Mockito.when(httpRequest.execute()).thenReturn(httpResponse);
        Mockito.when(httpResponse.getContent()).thenReturn(inputStream);


        httpRequestFactory2 = Mockito.mock(HttpRequestFactory.class);
        HttpRequest httpRequest2 = Mockito.mock(HttpRequest.class);
        HttpResponse httpResponse2 = Mockito.mock(HttpResponse.class);

        InputStream inputStream2 = new ByteArrayInputStream((json + "\n" + json2 + "\n" + json3 + "\n").getBytes());

        Mockito.when(httpRequestFactory2.buildPostRequest(Mockito.any(), Mockito.any())).thenReturn(httpRequest2);
        Mockito.when(httpRequest2.execute()).thenReturn(httpResponse2);
        Mockito.when(httpResponse2.getContent()).thenReturn(inputStream2);
    }

    @Test
    public void getIterator() throws Exception {
        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        List<String> c = new ArrayList<>();
        it.forEachRemaining(c::add);
        Assert.assertEquals(json, c.get(0));
        Assert.assertEquals(json2, c.get(1));

    }

    @Test
    public void getUsers() throws Exception {

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        Collection<User> users = t.getUsers(new TwitterIterator(it, new TwitterPredicateFilter(5, 5)));

        User user = new User(2641246885L, "Mario", "maryh_collins", dateFormat.parse("Mon Jul 14 00:36:34 +0000 2014"));
        user.addTweet(new Tweet(807606539146104835L, dateFormat.parse("Sat Dec 10 15:22:58 +0000 2016"), "testing text bieber"));

        User user2 = new User(2264508682L, "test", "dotadobieber", dateFormat.parse("Mon Jan 06 00:58:03 +0000 2014"));
        user2.addTweet(new Tweet(807617536414126080L, dateFormat.parse("Sat Dec 10 16:06:40 +0000 2016"), "test"));


        Assert.assertTrue(Arrays.asList(user, user2).containsAll(users));
    }

    @Test
    public void getUsersTwoTweetsPerUser() throws Exception {

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory2, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        Collection<User> users = t.getUsers(new TwitterIterator(it, new TwitterPredicateFilter(5, 5)));

        User user = new User(2641246885L, "Mario", "maryh_collins", dateFormat.parse("Mon Jul 14 00:36:34 +0000 2014"));
        user.addTweet(new Tweet(807606539146104835L, dateFormat.parse("Sat Dec 10 15:22:58 +0000 2016"), "testing text bieber"));

        User user2 = new User(2264508682L, "test", "dotadobieber", dateFormat.parse("Mon Jan 06 00:58:03 +0000 2014"));
        user2.addTweet(new Tweet(807617536414126080L, dateFormat.parse("Sat Dec 10 16:06:40 +0000 2016"), "test"));
        user2.addTweet(new Tweet(807617536414126084L, dateFormat.parse("Sat Dec 10 16:06:40 +0000 2015"), "test"));

        Assert.assertTrue(Arrays.asList(user, user2).containsAll(users));

        User test = users.stream().filter(u -> u.getName().equals("test")).findFirst().orElseThrow(() -> new RuntimeException("Empty."));
        Assert.assertEquals(user2, test);
    }

    @Test
    public void sortedUserStringIterator() throws Exception {

        String first = "User-> name:Mario, screenName:maryh_collins, id:2641246885, createdAt:1405298194000 " +
                "\tTweet-> id:807606539146104835, createdAt:1481383378000, text:testing text bieber ";

        String second = "User-> name:test, screenName:dotadobieber, id:2264508682, createdAt:1388969883000 " +
                "\tTweet-> id:807617536414126080, createdAt:1481386000000, text:test " +
                "User-> name:Mario, screenName:maryh_collins, id:2641246885, createdAt:1405298194000 " +
                "\tTweet-> id:807606539146104835, createdAt:1481383378000, text:testing text bieber ";

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");
        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(2, 5);
        TwitterIterator twitterIterator = new TwitterIterator(it, twitterPredicateFilter);


        SortedStringTwitterIterator s = new SortedStringTwitterIterator(twitterIterator, Comparator.comparing(Tweet::getCreatedAt));
        List<String> collect = t.sortedUserStringStream(s).collect(Collectors.toList());



        Assert.assertEquals(first, stripLineBreaks(collect.get(0)));
        Assert.assertEquals(second, stripLineBreaks(collect.get(1)));
    }

    @Test
    public void sortedUserIterator() throws Exception {

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(2, 5);

        TwitterIterator twitterIterator = new TwitterIterator(it, twitterPredicateFilter);

        SortedTwitterIterator s = new SortedTwitterIterator(twitterIterator);
        List<List<User>> collect = t.sortedUserStream(s).collect(Collectors.toList());

        User user = new User(2641246885L, "Mario", "maryh_collins", dateFormat.parse("Mon Jul 14 00:36:34 +0000 2014"));
        user.addTweet(new Tweet(807606539146104835L, dateFormat.parse("Sat Dec 10 15:22:58 +0000 2016"), "testing text bieber"));

        User user2 = new User(2264508682L, "test", "dotadobieber", dateFormat.parse("Mon Jan 06 00:58:03 +0000 2014"));
        user2.addTweet(new Tweet(807617536414126080L, dateFormat.parse("Sat Dec 10 16:06:40 +0000 2016"), "test"));

        Assert.assertEquals(Arrays.asList(user), collect.get(0));
        Assert.assertEquals(Arrays.asList(user2, user), collect.get(1));
    }

    @Test
    public void predicateFilter() throws Exception {

        String first = "User-> name:Mario, screenName:maryh_collins, id:2641246885, createdAt:1405298194000\n" +
                "\tTweet-> id:807606539146104835, createdAt:1481383378000, text:testing text bieber\n";

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(1, 5);

        TwitterIterator twitterIterator = new TwitterIterator(it, twitterPredicateFilter);

        SortedStringTwitterIterator s = new SortedStringTwitterIterator(twitterIterator, Comparator.comparing(Tweet::getCreatedAt));
        List<String> collect = t.sortedUserStringStream(s).collect(Collectors.toList());

        Assert.assertEquals(first, collect.get(0));
        Assert.assertEquals(1, collect.size());
    }

    @Test
    public void printSortedUsers() throws Exception {

        String expected = "User-> name:test, screenName:dotadobieber, id:2264508682, createdAt:1388969883000 " +
                "\tTweet-> id:807617536414126080, createdAt:1481386000000, text:test " +
                "User-> name:Mario, screenName:maryh_collins, id:2641246885, createdAt:1405298194000 " +
                "\tTweet-> id:807606539146104835, createdAt:1481383378000, text:testing text bieber ";
        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);

        TwitterPredicateFilter predicateFilter = new TwitterPredicateFilter(2, 5);
        Set<User> users = t.getUsers(new TwitterIterator(it, predicateFilter));

        Printer.printSortedUsers(users, printStream,
                Comparator.comparing(User::getCreatedAt), Comparator.comparing(Tweet::getCreatedAt));

        String content = new String(out.toByteArray(), StandardCharsets.UTF_8);

        Assert.assertEquals(expected, stripLineBreaks(content));
    }

    @Test
    public void printSortedUsersReverse() throws Exception {

        String expected =
                "User-> name:Mario, screenName:maryh_collins, id:2641246885, createdAt:1405298194000 " +
                        "\tTweet-> id:807606539146104835, createdAt:1481383378000, text:testing text bieber " +
                        "User-> name:test, screenName:dotadobieber, id:2264508682, createdAt:1388969883000 " +
                        "\tTweet-> id:807617536414126080, createdAt:1481386000000, text:test ";

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);

        TwitterPredicateFilter predicateFilter = new TwitterPredicateFilter(2, 5);
        Set<User> users = t.getUsers(new TwitterIterator(it, predicateFilter));

        Printer.printSortedUsers(users, printStream,
                Comparator.comparing(User::getCreatedAt).reversed(), Comparator.comparing(Tweet::getCreatedAt).reversed());

        String content = new String(out.toByteArray(), StandardCharsets.UTF_8);

        Assert.assertEquals(expected, stripLineBreaks(content));
    }


    @Test
    public void predicateFilter_zeroTweets() throws Exception {

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");


        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(0, 5);
        TwitterIterator twitterIterator = new TwitterIterator(it, twitterPredicateFilter);
        SortedStringTwitterIterator s = new SortedStringTwitterIterator(twitterIterator, Comparator.comparing(Tweet::getCreatedAt));
        List<String> collect = t.sortedUserStringStream(s).collect(Collectors.toList());
        Assert.assertEquals(0, collect.size());
    }

    @Test
    public void predicateFilter_zeroTime() throws Exception {

        TwitterStreamer t = new TwitterStreamer("http://mock.url", httpRequestFactory, 90000);

        Iterator<String> it = t.getSimpleStringIterator("bieber");

        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(2, 0);
        TwitterIterator twitterIterator = new TwitterIterator(it, twitterPredicateFilter);
        SortedStringTwitterIterator s = new SortedStringTwitterIterator(twitterIterator, Comparator.comparing(Tweet::getCreatedAt));
        List<String> collect = t.sortedUserStringStream(s).collect(Collectors.toList());
        Assert.assertEquals(0, collect.size());
    }

    public String stripLineBreaks(String s) {
        return s.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ");
    }


}
