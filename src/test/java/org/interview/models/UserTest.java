package org.interview.models;

import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserTest {

    private static final String pattern = "EEE MMM d HH:mm:ss zzz yyyy";
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

    @Test
    public void userFromParams() throws Exception {

        // Given some user date
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = new Date(1234);

        // When we instantiate a new user
        User user = new User(id, name, screenName, createdAt);

        // Then should return the correct data
        assertEquals(id, user.getId());
        assertEquals(name, user.getName());
        assertEquals(screenName, user.getScreenName());
        assertEquals(createdAt, user.getCreatedAt());
    }

    @Test
    public void userFromJson() throws Exception {

        // Given a JsonObject with the correct structure
        String dateString = "Sat Dec 10 13:31:42 +0000 2016";
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = dateFormat.parse(dateString);

        JsonObject o = new JsonObject();
        o.addProperty("id", id);
        o.addProperty("created_at", dateString);
        o.addProperty("name", name);
        o.addProperty("screen_name", screenName);


        // When creating a new User from the JsonObeject
        User user = new User(o);

        // Should return the correctly created User
        assertEquals(id, user.getId());
        assertEquals(name, user.getName());
        assertEquals(screenName, user.getScreenName());
        assertEquals(createdAt, user.getCreatedAt());
    }

    @Test
    public void userFromParamsWithTweets() throws Exception {


        //Given user data and some tweets
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = new Date(1234);

        Tweet tweet = new Tweet(1L, new Date(2000), "my mock text");
        Tweet anotherTweet = new Tweet(2L, new Date(3000), "my other mock text");

        List<Tweet> tweets = Arrays.asList(tweet, anotherTweet);

        // When creating an user with existing tweets
        User user = new User(id, name, screenName, createdAt, tweets);


        // Should return the correct user and the tweets
        assertEquals(id, user.getId());
        assertEquals(name, user.getName());
        assertEquals(screenName, user.getScreenName());
        assertEquals(createdAt, user.getCreatedAt());
        assertEquals(tweets, user.getTweets());

    }

    @Test
    public void userFromJsonWithTweet() throws Exception {

        //Given user data and some tweets
        String dateString = "Sat Dec 10 13:31:42 +0000 2016";

        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = dateFormat.parse(dateString);

        JsonObject o = new JsonObject();
        o.addProperty("id", id);
        o.addProperty("created_at", dateString);
        o.addProperty("name", name);
        o.addProperty("screen_name", screenName);

        Tweet tweet = new Tweet(1L, new Date(2000), "my mock text");
        Tweet anotherTweet = new Tweet(2L, new Date(3000), "my other mock text");

        List<Tweet> tweets = Arrays.asList(tweet, anotherTweet);

        // When creating an user from JSON with existing tweets
        User user = new User(o, tweets);

        // Should return the correct user and the tweets
        assertEquals(id, user.getId());
        assertEquals(name, user.getName());
        assertEquals(screenName, user.getScreenName());
        assertEquals(createdAt, user.getCreatedAt());
        assertEquals(tweets, user.getTweets());
    }

    @Test
    public void addTweet() throws Exception {

        // Given an user and tweet
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = new Date(1234);

        User user = new User(id, name, screenName, createdAt);

        // When adding new tweet
        Tweet tweet = new Tweet(1L, new Date(2000), "my mock text");
        user.addTweet(tweet);

        // Should add the tweet to the users Collection<Tweet>
        Assert.assertEquals(1, user.getTweets().size());
        Assert.assertEquals(tweet, user.getTweets().stream().findFirst().orElseThrow(() -> new RuntimeException("No tweets found in Collection.")));
    }

    @Test
    public void addTweets() throws Exception {

        // Given an user and multiple tweets
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = new Date(1234);

        User user = new User(id, name, screenName, createdAt);

        // When adding new tweets
        Tweet tweet = new Tweet(1L, new Date(2000), "my mock text");
        Tweet anotherTweet = new Tweet(2L, new Date(3000), "my other mock text");

        List<Tweet> tweets = Arrays.asList(tweet, anotherTweet);

        user.addTweet(tweets);

        // Should add the tweets to the users Collection<Tweet>
        Assert.assertEquals(tweets.size(), user.getTweets().size());
        Assert.assertEquals(tweets, user.getTweets());
    }

    @Test
    public void compareTo() throws Exception {

        // Given two users with different creation dates
        long id = 1;
        String name = "picnic";
        String screenName = "Picnic.nl";
        Date createdAt = new Date(1234);

        long olderId = 1;
        String olderName = "picnic";
        String olderScreenName = "Picnic.nl";
        Date olderCreatedAt = new Date(1);

        User user = new User(id, name, screenName, createdAt);
        User oldUser = new User(olderId, olderName, olderScreenName, olderCreatedAt);

        // When comparing the users, should return:
        // Greater than 0 if this user is older than that user
        // Smaller than 0 if this user is younger than that user
        // Zero if users share the same Date createdAt value.
        assertTrue(user.compareTo(oldUser) > 0);
        assertTrue(oldUser.compareTo(user) < 0);
        assertTrue(user.compareTo(user) == 0);

    }
}
