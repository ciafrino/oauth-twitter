package org.interview.models;

import com.google.gson.JsonObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TweetTest {
    private static final String pattern = "EEE MMM d HH:mm:ss zzz yyyy";
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

    @Test
    public void tweetFromParams() throws Exception {

        // Given a single tweet data
        long id = 1;
        Date createdAt = new Date(1234);
        String text = "my mock text";

        // When creating the tweet
        Tweet tweet = new Tweet(id, createdAt, text);

        // Should return tweet
        assertEquals(id, tweet.getId());
        assertEquals(createdAt, tweet.getCreatedAt());
        assertEquals(text, tweet.getText());
    }

    @Test
    public void tweetFromJson() throws Exception {

        // Given properly structured JsonObject
        String dateString = "Sat Dec 10 13:31:42 +0000 2016";
        JsonObject o = new JsonObject();
        o.addProperty("id", 1L);
        o.addProperty("created_at", dateString);
        o.addProperty("text", "my mock text");

        // When creating tweet
        Tweet tweet = new Tweet(o);

        // Should return expected fields
        assertEquals(1L, tweet.getId());
        assertEquals(dateFormat.parse(dateString), tweet.getCreatedAt());
        assertEquals("my mock text", tweet.getText());
    }
}
