package org.interview.utils;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.io.IOException;

/**
 * Class containing the configuration of the command line interface.
 */
public class CommandLineArgParser {

    private OptionSet options;
    private OptionParser optionParser = new OptionParser();

    private OptionSpec<String> filter = optionParser.accepts("filter")
            .withOptionalArg()
            .ofType(String.class)
            .defaultsTo("bieber")
            .describedAs("String to filter tweets on.");

    private OptionSpec<Integer> maxTweets = optionParser.accepts("tweets")
            .withOptionalArg()
            .ofType(Integer.class)
            .defaultsTo(100)
            .describedAs("Maximum number of tweets to stream.");

    private OptionSpec<Integer> maxTime = optionParser.accepts("time")
            .withOptionalArg()
            .ofType(Integer.class)
            .defaultsTo(30)
            .describedAs("Maximum elapsed time streaming.");

    private OptionSpec<Boolean> dynamicPrint = optionParser.accepts("dynamicPrint")
            .withOptionalArg()
            .ofType(Boolean.class)
            .defaultsTo(true)
            .describedAs("Print sorting dynamically");

    private OptionSpec<String> fileName = optionParser.accepts("fileName")
            .withOptionalArg()
            .ofType(String.class)
            .defaultsTo("emptyFileName")
            .describedAs("Write to System.out or file");

    private OptionSpec<Void> help = optionParser.accepts("help").forHelp();


    /**
     * Initialize a CommandLineArgParser with the main's args array.
     * @param args Array of arguments retrieved from the run options.
     */
    public CommandLineArgParser(String[] args) {
        options = optionParser.parse(args);
    }


    /**
     *
     * @return maximum amount of time spent streaming tweets.
     */
    public int getMaxTime() {
        return options.valueOf(maxTime);
    }

    /**
     *
     * @return maximum number of tweets that will be streamed.
     */
    public int getMaxTweets() {
        return options.valueOf(maxTweets);
    }

    /**
     *
     * @return string used to filter tweets.
     */
    public String getFilter() {
        return options.valueOf(filter);
    }

    /**
     *
     * @return wether to print tweets as they are streamed through, sorting them as they come using the User::compareTo
     * or wait for all the tweets to stream and print all at once.
     */
    public boolean getDynamicPrint() {
        return options.valueOf(dynamicPrint);
    }


    /**
     * Optional file name, if present the output of the stream will be redirected to a text file of name fileName.
     * @return fileName, if not set, returns 'emptyFileName'
     */
    public String getFileName() {
        return options.valueOf(fileName);
    }

    public void printHelp() throws IOException {
        optionParser.printHelpOn(System.out);
    }
}
