package org.interview.utils;

import org.interview.models.Tweet;
import org.interview.models.User;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

public class Printer {

    private Printer() {
    }

    public static void printSortedUsers(Collection<User> users, PrintStream printStream, Comparator<User> userComparator, Comparator<Tweet> tweetComparator) {

        for (User u : users.stream().sorted(userComparator).collect(Collectors.toList())) {
            printStream.println(u);
            for (Tweet t : u.getSortedTweets(tweetComparator)) {
                printStream.println("\t" + t);
            }
        }
    }
}
