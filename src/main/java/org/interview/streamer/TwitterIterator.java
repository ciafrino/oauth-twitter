package org.interview.streamer;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.interview.models.Pair;
import org.interview.models.Tweet;
import org.interview.models.User;

import java.text.ParseException;
import java.util.Iterator;

/**
 * Implementation of the Iterator interface. This implementation takes a Iterator of Twitter API JSON strings
 * and transforms that into a Iterator of a pair of User and Tweet, filtered by the specification in the TwitterPredicateFilter.
 */
public class TwitterIterator implements Iterator<Pair> {

    private TwitterPredicateFilter twitterPredicateFilter;
    private Iterator<String> twitterStringIterator;

    public TwitterIterator(Iterator<String> twitterStringIterator, TwitterPredicateFilter twitterPredicateFilter) {
        this.twitterStringIterator = twitterStringIterator;
        this.twitterPredicateFilter = twitterPredicateFilter;
    }

    @Override
    public boolean hasNext() {

        twitterPredicateFilter.startTimer();
        return twitterStringIterator.hasNext() && twitterPredicateFilter.test();

    }

    @Override
    public Pair next() {

        twitterPredicateFilter.incrementCounter();
        String tweetAsJsonString = twitterStringIterator.next();
        JsonObject tweetJsonObject = new JsonParser().parse(tweetAsJsonString).getAsJsonObject();
        JsonObject userJsonObject = tweetJsonObject.get("user").getAsJsonObject();

        User user = null;
        Tweet tweet = null;
        try {
            user = new User(userJsonObject);
            tweet = new Tweet(tweetJsonObject);
        } catch (ParseException e) {
            System.out.println(tweetJsonObject.toString());
            e.printStackTrace();
        }
        return new Pair(user, tweet);
    }
}
