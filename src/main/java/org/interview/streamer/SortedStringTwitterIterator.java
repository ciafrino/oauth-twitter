package org.interview.streamer;

import org.interview.models.Pair;
import org.interview.models.Tweet;
import org.interview.models.User;

import java.util.*;

/**
 * SortedStringTwitterIterator is an extension to the TwitterIterator, implementing the Iterator interface as well.
 * This Iterator returns snapshots of the stream up to the moment next() is called as a string,
 * with their respective tweets.
 *
 * This is done by keeping a SortedSet of User, sorted according to the User.compareTo() method and a HashMap of User,
 * used to keep a reference to the User, so we can add new tweets to that user object.
 */
public class SortedStringTwitterIterator implements Iterator<String> {

    private TwitterIterator twitterIterator;
    private SortedSet<User> userSortedSet;
    private Comparator<Tweet> comparator;
    private Map<Long, User> userMap;

    public SortedStringTwitterIterator(TwitterIterator twitterIterator, Comparator<Tweet> comparator) {
        this.twitterIterator = twitterIterator;
        this.comparator = comparator;
        this.userSortedSet = new TreeSet<>();
        this.userMap = new HashMap<>();

    }

    @Override
    public boolean hasNext() {
        return twitterIterator.hasNext();
    }

    @Override
    public String next() {

        StringBuilder sb = new StringBuilder();

        Pair pair = twitterIterator.next();
        User user = pair.getUser();
        Tweet tweet = pair.getTweet();

        long id = user.getId();
        if (userSortedSet.contains(user)) {
            userMap.get(id).addTweet(tweet);
        } else {
            user.addTweet(tweet);
            userMap.put(id, user);
            userSortedSet.add(userMap.get(id));
        }

        for (User u : userSortedSet) {
            sb.append(u).append("\n");
            for (Tweet t : u.getSortedTweets(comparator)) {
                sb.append("\t").append(t);
            }
            sb.append("").append("\n");
        }

        return sb.toString();
    }
}
