package org.interview.streamer;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.interview.models.Pair;
import org.interview.models.Tweet;
import org.interview.models.User;

import java.io.*;
import java.net.URI;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Class implementing methods to interact with a Twitter API stream originated in a HTTP request.
 * This class implements helper methods to receive Java 8 streams, Collections and Iterators.
 */
public class TwitterStreamer {

    private String twitterUrl;
    private HttpRequestFactory requestFactory;
    private int connectionTimeout;

    public TwitterStreamer(String twitterUrl, HttpRequestFactory httpRequestFactory, int connectionTimeout) {
        this.twitterUrl = twitterUrl;
        this.requestFactory = httpRequestFactory;
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * This method constructs the first consumable of the HTTP request.
     * @param filter the string used to track tweets
     * @return Iterator of Strings, where each String is JSON Tweet object in its String representation
     * @throws IOException delivered by the HTTP requests
     */
    public Iterator<String> getSimpleStringIterator(String filter) throws IOException {

        String url = String.format(twitterUrl, filter);
        GenericUrl genericUrl = new GenericUrl(URI.create(url));

        HttpRequest httpRequest = requestFactory.buildPostRequest(genericUrl, null);
        httpRequest.setReadTimeout(connectionTimeout);
        InputStream content = httpRequest.execute().getContent();

        return new BufferedReader(new InputStreamReader(content, Charset.forName("UTF-8")))
                .lines()
                .iterator();
    }

    /**
     * Given a Twitter iterator, acumulate the tweets and users, returning the digest of users with their respective tweets.
     * @param twitterIterator
     * @return Set of Users with all the tweets created by them in the stream duration.
     */
    public Set<User> getUsers(TwitterIterator twitterIterator) {

        Map<Long, User> users = new HashMap<>();

        while (twitterIterator.hasNext()) {
            Pair pair = twitterIterator.next();
            long id = pair.getUser().getId();

            if (users.containsKey(id)) {
                users.get(id).addTweet(pair.getTweet());
            }
            else {
                pair.getUser().addTweet(pair.getTweet());
                users.put(id, pair.getUser());
            }
        }
        return users.values().stream().collect(Collectors.toSet());
    }

    /**
     * Simple method to transform a sortedTwitterIterator into a Stream.
     * @param sortedTwitterIterator
     * @return Stream of Strings. Each String is a formatted digest of what happened in the stream up to that iteration.
     * containing all the users and their tweets.
     */
    public Stream<List<User>> sortedUserStream(SortedTwitterIterator sortedTwitterIterator) {
        Iterable<List<User>> iterable = () -> sortedTwitterIterator;
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    /**
     * Simple method to transform a sortedTwitterIterator into a Stream.
     * @param sortedStringTwitterIterator
     * @return Stream of Strings. Each String is a formatted digest of what happened in the stream up to that iteration.
     * containing all the users and their tweets.
     */
    public Stream<String> sortedUserStringStream(SortedStringTwitterIterator sortedStringTwitterIterator) {
        Iterable<String> iterable = () -> sortedStringTwitterIterator;
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
