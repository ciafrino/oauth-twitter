package org.interview.streamer;

import org.interview.models.Pair;
import org.interview.models.Tweet;
import org.interview.models.User;

import java.util.*;
import java.util.stream.Collectors;

/**
 * SortedTwitterIterator is an extension to the TwitterIterator, implementing the Iterator interface as well.
 * This Iterator returns snapshots of the stream up to the moment next() is called as a list of Users,
 * with their respective tweets.
 *
 * This is done by keeping a SortedSet of User, sorted according to the User.compareTo() method and a HashMap of User,
 * used to keep a reference to the User, so we can add new tweets to that user object.
 */
public class SortedTwitterIterator implements Iterator<List<User>> {

    private TwitterIterator twitterIterator;
    private SortedSet<User> userSortedSet;
    private Map<Long, User> userMap;

    public SortedTwitterIterator(TwitterIterator twitterIterator) {
        this.twitterIterator = twitterIterator;
        this.userSortedSet = new TreeSet<>();
        this.userMap = new HashMap<>();
    }

    @Override
    public boolean hasNext() {

        return twitterIterator.hasNext();
    }

    @Override
    public List<User> next() {

        Pair pair = twitterIterator.next();
        User user = pair.getUser();
        Tweet tweet = pair.getTweet();

        long id = user.getId();
        if (userSortedSet.contains(user)) {
            userMap.get(id).addTweet(tweet);
        } else {
            user.addTweet(tweet);
            userMap.put(id, user);
            userSortedSet.add(userMap.get(id));
        }
        return userSortedSet.stream().collect(Collectors.toList());
    }
}
