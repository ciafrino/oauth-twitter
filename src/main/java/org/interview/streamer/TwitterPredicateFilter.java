package org.interview.streamer;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Class to control an iteration based on outside parameters. The present implementation contains controls for
 * time limit and maximum iteration count. This allows for timed iterations, a feature not present in the Java 8 Streaming API.
 * This implementation is not thread safe: the memory footprint and iitialization costs are too slow to compensate for the
 * thread safety. Simply instantiate a new one for each usage.
 */
public class TwitterPredicateFilter {

    private AtomicLong counter = new AtomicLong(0L);
    private Instant start;
    private long maxTweets;
    private int maxTime;
    private AtomicBoolean hasStarted = new AtomicBoolean(false);

    /**
     * Initialize a new TwitterPredicateFilter
     * @param maxTweets maximum number of tweets to stream
     * @param maxTime maximum amount of time elapsed with the stream open
     */
    public TwitterPredicateFilter(long maxTweets, int maxTime) {
        this.maxTweets = maxTweets;
        this.maxTime = maxTime;
    }

    /**
     * Start timing the stream operation. This is a irreversible operation and repeated calls to the method will NOT
     * restart the timer.
     */
    public void startTimer() {
        if (!hasStarted.get()) {
            start = Instant.now();
            hasStarted.set(true);
        }
    }

    /**
     * Increment the internal counter. In this application scope, this is done once a tweet is successfully retrieved.
     */
    public void incrementCounter() {
        counter.incrementAndGet();
    }

    /**
     * The implementation of the Predicate used to control the iteration loop. The current logic depends on
     * the total elapsed time and number of tweets retrieved.
     * @return wether to continue or break the operation.
     */
    public boolean test() {
        return counter.get() < maxTweets && Math.abs(Duration.between(start, Instant.now()).getSeconds()) < maxTime;
    }
}
