package org.interview;

import com.google.api.client.http.HttpRequestFactory;
import org.interview.models.Tweet;
import org.interview.models.User;
import org.interview.oauth.twitter.TwitterAuthenticationException;
import org.interview.oauth.twitter.TwitterAuthenticator;
import org.interview.streamer.SortedStringTwitterIterator;
import org.interview.streamer.TwitterIterator;
import org.interview.streamer.TwitterPredicateFilter;
import org.interview.streamer.TwitterStreamer;
import org.interview.utils.CommandLineArgParser;
import org.interview.utils.Printer;

import java.io.*;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Properties;

public class Main {

    private static String lineSeparator = "-----------------------------------------------------------------------------------";

    public static void main(String[] args) {

        //CLI Help and parsing args
        System.out.println(lineSeparator);
        CommandLineArgParser commandLineArgParser = getCommandLineArgParser(args);
        System.out.println(lineSeparator);

        //Setting up properties
        Properties properties = getProperties();
        String consumerKey = properties.getProperty("consumerKey");
        String consumerSecret = properties.getProperty("consumerSecret");
        String twitterUrl = properties.getProperty("twitterUrl");
        String encoding = properties.getProperty("encoding");
        int connectionTimeout = Integer.parseInt(properties.getProperty("connectionTimeout"));

        //Authentication and HttpRequest
        HttpRequestFactory requestFactory = getHttpRequestFactory(consumerKey, consumerSecret, encoding);

        //TwitterStream initialization
        TwitterStreamer twitterStreamer = new TwitterStreamer(twitterUrl, requestFactory, connectionTimeout);
        TwitterPredicateFilter twitterPredicateFilter = new TwitterPredicateFilter(commandLineArgParser.getMaxTweets(), commandLineArgParser.getMaxTime());
        TwitterIterator twitterIterator = getTwitterIterator(commandLineArgParser.getFilter(), twitterPredicateFilter, twitterStreamer);

        //Print to file or System.out
        PrintStream printStream = getPrintStream(commandLineArgParser.getFileName());

        System.out.println("Starting Twitter stream");
        //Print results as they come or wait for stream to close

        if (commandLineArgParser.getDynamicPrint()) {
            twitterStreamer
                    .sortedUserStringStream(new SortedStringTwitterIterator(twitterIterator, Comparator.comparing(Tweet::getCreatedAt)))
                    .forEach(s -> {
                        clearConsole();
                        printStream.println(s);
                    });

        } else {
            Collection<User> users = twitterStreamer.getUsers(twitterIterator);
            Printer.printSortedUsers(users, printStream, Comparator.comparing(User::getCreatedAt), Comparator.comparing(Tweet::getCreatedAt));
        }
        System.out.println("Streaming finished");
        System.exit(0);
    }

    private static PrintStream getPrintStream(String fileName) {
        PrintStream printStream = null;
        if (fileName.equals("emptyFileName")) {
            printStream = new PrintStream(System.out);
        } else {
            try {
                printStream = new PrintStream(new FileOutputStream(fileName));
            } catch (FileNotFoundException e) {
                errorMessage("File not found exception, check the file name and path", e);
            }
        }
        return printStream;
    }

    private static TwitterIterator getTwitterIterator(String filter, TwitterPredicateFilter twitterPredicateFilter, TwitterStreamer twitterStreamer) {
        TwitterIterator twitterIterator = null;
        try {
            Iterator<String> stringIterator = twitterStreamer.getSimpleStringIterator(filter);
            twitterIterator = new TwitterIterator(stringIterator, twitterPredicateFilter);
        } catch (IOException e) {
            errorMessage("I/O Exception when reading Twitter stream", e);
        }
        return twitterIterator;
    }

    private static HttpRequestFactory getHttpRequestFactory(String consumerKey, String consumerSecret, String encoding) {
        TwitterAuthenticator twitterAuthenticator = null;
        try {
            twitterAuthenticator = new TwitterAuthenticator(new PrintStream(System.out, true, encoding), consumerKey, consumerSecret);
        } catch (UnsupportedEncodingException e) {
            errorMessage("Error while trying to use the ecoding set on properties file", e);
        }
        HttpRequestFactory requestFactory = null;
        try {
            requestFactory = twitterAuthenticator.getAuthorizedHttpRequestFactory();
        } catch (TwitterAuthenticationException e) {
            errorMessage("Twitter authetication error", e);
        }
        return requestFactory;
    }

    private static CommandLineArgParser getCommandLineArgParser(String[] args) {
        CommandLineArgParser commandLineArgParser = new CommandLineArgParser(args);
        try {
            commandLineArgParser.printHelp();
        } catch (IOException e) {
            errorMessage("Error while trying to access System.out", e);
        }
        return commandLineArgParser;
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(Main.class.getSimpleName() + ".properties"));
        } catch (IOException e) {
            errorMessage("Error while trying to load properties file", e);
        }
        return properties;
    }

    public static void clearConsole() {
        final String ANSI_CLS = "\u001b[2J";
        final String ANSI_HOME = "\u001b[H";
        System.out.print(ANSI_CLS + ANSI_HOME);
        System.out.flush();
    }

    private static void errorMessage(String message, Exception e) {
        System.out.println(message + ": " + e.getMessage() + "\n" + "Cause by: " + e.getCause());
    }

}