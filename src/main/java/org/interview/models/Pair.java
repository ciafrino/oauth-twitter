package org.interview.models;

/**
 * Simple container class to hold a User, Tweet pair.
 */
public class Pair {

    private User user;
    private Tweet tweet;

    public Pair(User user, Tweet tweet) {
        this.user = user;
        this.tweet = tweet;
    }

    public User getUser() {
        return user;
    }

    public Tweet getTweet() {
        return tweet;
    }
}
