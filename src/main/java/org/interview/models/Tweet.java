package org.interview.models;

import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Tweet class representing Twitter Streaming API Tweet JSON object.
 */
public class Tweet {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");

    private long id;
    private Date createdAt;
    private String text;


    /**
     * Initialize a Tweet using the required fields.
     * @param id Tweet id
     * @param createdAt Date of creation
     * @param text Text contained in the tweet
     */
    public Tweet(@Nonnull Long id, @Nonnull Date createdAt, @Nonnull String text) {
        this.id = id;
        this.createdAt = createdAt;
        this.text = text;
    }

    /**
     * Initialize a Tweet straight from the Twitter API JSON object.
     * @param tweetJsonObject Twitter JSON object containing the fields 'id', 'created_at', 'text'.
     * @throws ParseException Throws ParseException on JsonObjects missing the required fields or have inconsistent values.
     */
    public Tweet(@Nonnull JsonObject tweetJsonObject) throws ParseException {
        this.id = tweetJsonObject.get("id").getAsLong();
        this.createdAt = dateFormat.parse(tweetJsonObject.get("created_at").getAsString());
        this.text = tweetJsonObject.get("text").getAsString();
    }

    public long getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getText() {
        return text;
    }


    /**
     *
     * @return All the property fields of the tweet.
     */
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Tweet-> ");
        sb.append("id:").append(id);
        sb.append(", createdAt:").append(createdAt.getTime());
        sb.append(", text:")
                .append(
                        text.replaceAll("\n|\n\r|\r", " "));
                                //.substring(0, (text.length() < 80) ? text.length() : 80));
        return sb.toString();
    }


    /**
     * .equals method needs to be re-written in the event of adding more properties to the Tweet class
     * @param other Twitter to be compared to
     * @return Tweets must have the same id, createdAt and same text.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        Tweet tweet = (Tweet) other;

        if (id != tweet.id) return false;
        if (!createdAt.equals(tweet.createdAt)) return false;
        return text.equals(tweet.text);
    }
}
