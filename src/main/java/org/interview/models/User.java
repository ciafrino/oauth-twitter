package org.interview.models;

import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * User class representing Twitter Streaming API User JSON object.
 */
public class User implements Comparable<User> {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");

    private String name;
    private String screenName;
    private long id;
    private Date createdAt;
    private Collection<Tweet> tweets;

    /**
     * Initialize the User object using the required fields
     * @param id User id
     * @param name User account name
     * @param screenName User screenName
     * @param createdAt Date of creation
     */
    public User(@Nonnull Long id, @Nonnull String name, @Nonnull String screenName, @Nonnull Date createdAt) {
        this.id = id;
        this.name = name;
        this.screenName = screenName;
        this.createdAt = createdAt;
        tweets = new ArrayList<>();
    }

    /**
     * Initialize the User object using the required fields and add some tweets to it
     * @param id User id
     * @param name User account name
     * @param screenName User screenName
     * @param createdAt Date of creation
     * @param tweets Collection of Tweets owned by this user
     */
    public User(long id, @Nonnull String name, @Nonnull String screenName, @Nonnull Date createdAt, @Nonnull Collection<Tweet> tweets) {
        this.id = id;
        this.name = name;
        this.screenName = screenName;
        this.createdAt = createdAt;
        this.tweets = new ArrayList<>();
        addTweet(tweets);
    }

    /**
     * Initialize a User straight from the Twitter API JSON object.
     * @param userTwitterJsonObject Containing the required fields 'name', 'screen_name', 'id' and 'created_at'
     * @throws ParseException throws ParseException if any of the required fields are missing or have inconsistent values.
     */
    public User(@Nonnull JsonObject userTwitterJsonObject) throws ParseException {

        name = userTwitterJsonObject.get("name").getAsString();
        screenName = userTwitterJsonObject.get("screen_name").getAsString();
        id = userTwitterJsonObject.get("id").getAsLong();
        createdAt = dateFormat.parse(userTwitterJsonObject.get("created_at").getAsString());
        tweets = new ArrayList<>();
    }

    /**
     * Initialize a User straight from the Twitter API JSON object and add some tweets.
     * @param userTwitterJsonObject Containing the required fields 'name', 'screen_name', 'id' and 'created_at'
     * @param tweets Collection of tweets to be added to this User
     * @throws ParseException throws ParseException if any of the required fields are missing or have inconsistent values.
     */
    public User(@Nonnull JsonObject userTwitterJsonObject, @Nonnull Collection<Tweet> tweets) throws ParseException {

        name = userTwitterJsonObject.get("name").getAsString();
        screenName = userTwitterJsonObject.get("screen_name").getAsString();
        id = userTwitterJsonObject.get("id").getAsLong();
        createdAt = dateFormat.parse(userTwitterJsonObject.get("created_at").getAsString());
        this.tweets = new ArrayList<>();
        addTweet(tweets);
    }

    public void addTweet(Tweet tweet) {
        tweets.add(tweet);
    }

    public void addTweet(Collection<Tweet> tweets) {
        this.tweets.addAll(tweets);
    }

    public Collection<Tweet> getTweets() {
        return tweets;
    }

    public List<Tweet> getSortedTweets(Comparator<Tweet> comparator) {return tweets.stream().sorted(comparator).collect(Collectors.toList());}

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }

    public long getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User-> ");
        sb.append("name:").append(name);
        sb.append(", screenName:").append(screenName);
        sb.append(", id:").append(id);
        sb.append(", createdAt:").append(createdAt.getTime());

        return sb.toString();
    }

    /**
     * User compareTo method, implemented comparing the createdAt fields.
     * @param other other User to compare to
     * @return returns Date::compareTo of the users createdAt field.
     */
    @Override
    public int compareTo(User other) {
        return this.getCreatedAt().compareTo(other.createdAt);
    }

    /**
     * Method to compare the equality of two users.
     * .equals method needs to be re-written in the event of adding more properties to the User class
     * @param other Other use to compare
     * @return true if all the fields are equal and both hold the same tweets in their tweet collection
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        User user = (User) other;

        if (id != user.id) return false;
        return true;
    }
}
