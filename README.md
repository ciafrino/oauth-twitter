# README #

All the required functionalities have been implemented, alongside with some extensions. 
Running the project is straight-forward as the project remains self-contained.

The usual `mvn clean package` and `java -jar oauth-twitter-1.0-jar-with-dependencies.jar` will do.

Cool .gif of the dynamic printing on the standard Ubuntu terminal emulator at the project root.

## Output Files ##
The output of the program can be found in two files at the project root: `dynamic.txt` and `non-dynamic.txt`. 
The first contains several blocks of tweets, each block separated by a empty line. Each block is the accumulation of all the tweets streamed up to that iteration, sorted
dynamically using the `TwitterSortedStringIterator`.
The second file contains one single block of tweets. In this case the stream was accumulated during the whole streaming time and then finally sorted according to the given criteria.

## Functionality ##

+ Command-line options using [JOpt Simple](https://pholser.github.io/jopt-simple/)
  * --dynamicPrint (default = true)
      * Print the users, with their respective tweets, sorted in ascending chronological order.
         The sorting happens as the tweets stream and the results are updated on screen. There is a
         console emulator trick in place that might not work. If that happens, the tweets will come in
         blocks that increase in size and the sorting will be respected within such blocks.
      * If false, streams all the tweets, collects them, sorts and displays the whole result in one
        final operation.
        
  * --fileName
      * Prints the stream to the standard out.
      * If any file name is supplied, writes the output to the file.
  * --filter (default = _bieber_)
      * If another value is supplied, the stream will track that value.
  * --time (default = 30)
      * Maximum time _in seconds_ to keep the stream open.
  * --tweets (default = 100)
      * Maximum number of tweets to stream.      
## Design ##
The aim of this code architecture was to abstract the underlying HTTP façade and the Twitter JSON API.
This was done thought the use of the `Iterator<T>` interface, with a basic implementation that returns a 
`Iterator<Pair>`, where `Pair` is a container class for a pair of `User, Tweet`. This Iterator can then be 
converted in a `Stream<Pair>` or used as the Iterator in itself.

The main challenge with this was to abstract the control logic demanded in the specification. This was resolved
with the `TwitterPredicateFilter`. The time and tweet count limit can be easily extended in this single class.

Flexibility was planned by design. Most of the comparators are not hard-coded, being passed to the classes using them. The exception lies 
in the `User` class, where de `compareTo` method had to be written to be used by the `SortedSet`.

Additional functionality related to live sorting of the Twitter stream was implemented in the `SortedTwitterIterator`
and the `SortedStringTwitterIterator`. These are accessible through the `TwitterStream` class with 
Java 8 ``stream`` façades. The live sorting functionality was implemented using the `SortedSet` implementation
`TreeSet` and a `Map` for keeping references from User id's to User objects.
         
## Tools ##
The project was written using Intellij IDEA version 2016.3 Community Edition and tested in Windows 10 and
Ubuntu 16 environments.

Besides the included `google-oauth-client`, I have used some staple libraries in Java programming, those are:

  * `mockito 2.3.0`, with the optional functionality of final class mocking enable through
    the `org.mockito.plugins.MockMaker` in the `src/test/resources/mockito-extensions` directory. Note that this
    is only available in versions from version `2.x`.  
  * `junit 4.12` for running tests.
  * `jopt-simple 5.0.3` for the command line interface.
  * `gson 2.8.0` for JSON related functionality.
  
## Conclusion and Observations ##

I found this assignment very interesting and learned a lot from it, as I had never used the
Twitter Streaming API. I enjoy engineering good designs and the use of HTTP streams are a good opportunity to
apply interesting techniques and exploit the new Java 8 Stream API, as well as have a 
reflection about the difficulty in implementing the missing `takeWhile` method from the 
Java Stream API. The `TwitterPredicateFilter` was my humble and quick try at this kind of 
idea.

I studied the supplied class `TwitterAuthetication` and noticed a possible small bug within the 
part of the code responsible for the Twitter API key input. That is the `scanner.close()`
invokation on line 74. I have added a comment above it. 
I am not sure why the `System.in` stream was closed, as other classes interacting with your class will usually assume that
the `System.in` is open.